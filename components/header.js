export default () => <div className="header">
    <div className="pic">
        <div className="logo-wrapper">
            <a href="" id="logo">
                <img src="https://static.lufaxcdn.com/lufaxholding/common/images/logo.80d5324a.jpg" alt="lufax" />
            </a>
            <a href="" id="logo_white" style={{ display: 'none' }}>
                <img src="https://static.lufaxcdn.com/lufaxholding/common/images/logo_white.02384774.jpg" alt="lufax" />
            </a>
        </div>
    </div>
    <div className="nav">
        {' '}
        <Link href="/home">
            <a href="">首页</a>
        </Link>{' '}
        {' '}
        <Link href="/about">
            <a href="">关于我们</a>
        </Link>{' '}
        {' '}
        <Link href="/business">
            <a href="">业务与服务</a>
        </Link>{' '}
        {' '}
        <Link href="/news">
            <a href="">新闻中心</a>
        </Link>{' '}
        {' '}
        <Link href="/investor">
            <a href="">投资者关系</a>
        </Link>{' '}
        {' '}
        <Link href="/relation">
            <a href="">联系我们</a>
        </Link>{' '}
        {' '}
        <Link href="/joinus">
            <a href="">加入我们</a>
        </Link>{' '}
        {' '}
        <p><a href="" data-lan="zh-cn">简</a>
            <span>/</span>
            <a href="" data-lan="en-us">EN</a></p>
        {' '}
    </div>
</div>