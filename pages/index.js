
import Link from 'next/link'
export default () =>
    <div className="Indexcontainer">
        <style global jsx>{`
       *{
            margin: 0;
            padding: 0;
            list-style: none;
        }
        html, body {
            width: 100%;
            height: 100%;
        }
        #__next{
            width: 100%;
            height: 100%;
        }
        a{
            font-family: PingFangSC-Medium;
            font-weight: 600;
            font-size: 16px;
            color: #333;
            text-decoration: none;
        }
        a:hover {
            border-bottom: 2px solid #1266F6;
            color: #1266F6;
        }
        a :nth-child(2) :hover {
            
        }
        .Indexcontainer{
            width: 100%;
            height: 100%;
        }
        .header{
           width: 100%;
           height: 80px;
           display: flex;
           align-items: center;
           justify-content: space-around;
           border-bottom: 2px solid #000;
        }
        .nav{
            width:80%;
            height:100%;
            display: flex;
            align-items: center;
            justify-content: space-around;
        }
        .pic{
            flex:1;
            display: flex;
            justify-content: center;
        }
        .pic img{
            width:100px;
            height:50px;
        }
      `}</style>
        <div className="header">
            <div className="pic">
                <div className="logo-wrapper">
                    <a href="" id="logo">
                        <img src="https://static.lufaxcdn.com/lufaxholding/common/images/logo.80d5324a.jpg" alt="lufax"/>
                    </a>
                    <a href="" id="logo_white" style={{display:'none'}}>
                        <img src="https://static.lufaxcdn.com/lufaxholding/common/images/logo_white.02384774.jpg" alt="lufax"/>
                    </a>
                </div>
            </div>
            <div className="nav">
                {' '}
                <Link href="/home">
                    <a href="">首页</a>
                </Link>{' '}
                {' '}
                <Link href="/about">
                    <a href="">关于我们</a>
                </Link>{' '}
                {' '}
                <Link href="/business">
                    <a href="">业务与服务</a>
                </Link>{' '}
                {' '}
                <Link href="/news">
                    <a href="">新闻中心</a>
                </Link>{' '}
                {' '}
                <Link href="/index/investor">
                    <a href="">投资者关系</a>
                </Link>{' '}
                {' '}
                <Link href="/index/relation">
                    <a href="">联系我们</a>
                </Link>{' '}
                {' '}
                <Link href="/index/joinus">
                    <a href="">加入我们</a>
                </Link>{' '}
                {' '}
                <p><a href="" data-lan="zh-cn">简</a>
                    <span>/</span>
                    <a href="" data-lan="en-us">EN</a></p>
                {' '}
            </div>
        </div>
        <div className="Viewcontainer">

        </div>
    </div>


